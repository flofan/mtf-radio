﻿using System.Linq;
using RadioForMTF;
using RadioForMTF.API;
using Smod2;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem.Events;

namespace Handler
{
    internal class GlobalHandler : IEventHandlerTeamRespawn, IEventHandlerPlayerDie, IEventHandlerGeneratorFinish, IEventHandlerSpawn
    {
        private Main main;
        private string ntfunit;

        public GlobalHandler(Main main)
        {
            this.main = main;
        }

        public void OnGeneratorFinish(GeneratorFinishEvent ev)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("mtfr_generator", true))
            {
                API.SendMsgToMTF(LangManager.Manager.GetTranslation("rmtf.msg.generator.finish").Replace("\\n", "\n"));
            }
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            if (ev.Player.TeamRole.Team == Smod2.API.Team.NINETAILFOX)
            {
                if (ConfigManager.Manager.Config.GetBoolValue("mtfr_mtfdie", true))
                {
                    string msg = LangManager.Manager.GetTranslation("rmtf.msg.dead.mtf").Replace("\\n", "\n");
                    msg = msg.Replace("[victim]", ev.Player.Name);
                    msg = msg.Replace("[nb_alive]", PluginManager.Manager.Server.GetPlayers().Where(player => player.TeamRole.Team == Smod2.API.Team.NINETAILFOX).Count().ToString());

                    API.SendMsgToMTF(msg);
                }
            }
            else if(ev.Player.TeamRole.Team == Smod2.API.Team.SCP)
            {
                if (ConfigManager.Manager.Config.GetBoolValue("mtfr_scpdie", true))
                {
                    string msg = LangManager.Manager.GetTranslation("rmtf.msg.dead.scp").Replace("\\n", "\n");
                    msg = msg.Replace("[scp]", ev.Player.TeamRole.Role.ToString());
                    msg = msg.Replace("[nb_scpalive]", PluginManager.Manager.Server.GetPlayers().Where(player => player.TeamRole.Team == Smod2.API.Team.SCP).Count().ToString());
                    API.SendMsgToMTF(msg);
                }
            }
        }

        public void OnSpawn(PlayerSpawnEvent ev)
        {
            if(ev.Player.TeamRole.Role == Smod2.API.Role.FACILITY_GUARD)
            {

                ev.Player.PersonalBroadcast(5, LangManager.Manager.GetTranslation("rmtf.msg.spawn").Replace("\\n", "\n"), true);
            }
        }

        public void OnTeamRespawn(TeamRespawnEvent ev)
        {
            if (ev.SpawnChaos)
            {
                if (ConfigManager.Manager.Config.GetBoolValue("mtfr_cispawn", true))
                {
                    API.SendMsgToMTF(LangManager.Manager.GetTranslation("rmtf.msg.respawn.ci").Replace("\\n", "\n"));
                }
            }
            else
            {
                if (ConfigManager.Manager.Config.GetBoolValue("mtfr_mtfspawn", true))
                {
                    string msg = LangManager.Manager.GetTranslation("rmtf.msg.respawn.mtf").Replace("\\n", "\n");
                    msg = msg.Replace("[nb_mtf_count]", ev.PlayerList.Count.ToString());
                    API.SendMsgToMTF(msg);
                }
            }
        }
    }
}